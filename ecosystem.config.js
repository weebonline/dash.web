module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : "API",
      script    : "tcp.js",
      env: {
        COMMON_VARIABLE: "true"
      },
      env_production : {
        NODE_ENV: "production"
      }
    },

    // Second application
    {
      name      : "WEB",
      script    : "http.js"
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    local : {
      user : "tomasz",
      key  : "/home/tomasz/.ssh/id_rsa.pub",
      // Multi host in a js array
      host : ["10.10.10.10", "10.10.10.100", "10.5.0.55"],
      ref  : "master",
      // "repo" : "git@github.com:gallna/pm2-cluster.git",
      repo : "git@bitbucket.org:weebonline/dash.web.git",
      path : "/srv/pm2",
      // 'commands or local script path to be run on the host before the setup process starts'
      "pre-setup" : "npm install",
      // 'commands or a script path to be run on the host after cloning the repo'
      "post-setup": "bower install",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.json --env production",
      // This is a local executed command
      "pre-deploy-local" : ""
    }
  }
}
