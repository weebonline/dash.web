# web-v1
FROM gallna/node:v2

# Copy application folder and create volume
ADD . /app
# Define working directory.
WORKDIR /app

CMD    ["npm", "start"]
