(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('navItems', NavItems)
        .directive('navItem', NavItem);

    function NavItems() {
        return {
            restrict: 'AE',
            scope: {navItems: '=items'},
            controller: ['$scope', function NavItemsController($scope) {
                $scope.navItems = $scope.navItems.concat(navItems);

                var activeItem = {'isActive': false};
                var activeSubitem = {'isActive': false};

                this.addSubitem = function(subItem) {
                    subItem.isActive = true;
                    activeSubitem.isActive = false;
                    activeSubitem = subItem;
                }
                this.addItem = function(item) {
                    activeItem.isActive = false;
                    activeSubitem.isActive = false;
                    item.isActive = true;
                    activeItem = item;
                };
            }],
            templateUrl: 'widgets/sidebar/nav-items.html'
        };
    }

    function NavItem() {
        return {
            require: '^^navItems',
            restrict: 'AE',
            transclude: true,
            scope: {
                subItems: '=items',
                isActive: '=active',
                name: '=name',
                slug: '=slug',
                icon: '=icon'
            },
            link: function (scope, element, attrs, itemsCtrl) {
                if (scope.isActive === true) {
                    itemsCtrl.addItem(scope);
                    angular.forEach(scope.subItems, function(subItem) {
                        if (subItem.isActive === true) {
                            itemsCtrl.addSubitem(subItem);
                        }
                    });
                }
                scope.clicked = itemsCtrl.addItem;
                scope.subClicked = itemsCtrl.addSubitem;
            },
            templateUrl: 'widgets/sidebar/nav-item.html'
        };
    }




    var navItems = [
        {
            'slug': 'learn',
            'name': 'learn',
            'icon': 'l-basic-world',
            'isActive': false,
            'subItems': [
                {
                    'slug': 'tv',
                    'name': 'Tv',
                    'isActive': false
                },
                {
                    'slug': 'tablet',
                    'name': 'Tablet',
                    'isActive': false
                },
                {
                    'slug': 'smartphone',
                    'name': 'Smartphone',
                    'isActive': false
                }
            ]
        },
        {
            'slug': 'learn/tv',
            'name': 'Tv',
            'icon': 'l-basic-display',
            'isActive': false,
        },
        {
            'slug': 'learn/tablet',
            'name': 'Tablet',
            'icon': 'l-basic-tablet',
            'isActive': false,
            'subItems': []
        },
        {
            'slug': 'learn/smartphone',
            'name': 'Smartphone',
            'icon': 'l-basic-smartphone',
            'isActive': false,
            'subItems': []
        }
    ];


})();
