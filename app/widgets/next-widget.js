(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('nextWidget', NextWidget);

    function NextWidget(library) {
        return {
            scope: {
                channels: '=channels',
                schedules: '=schedules'
            },
            templateUrl: 'widgets/next-widget.html'
        };
    }
})();
