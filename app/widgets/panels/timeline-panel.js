(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('timelinePanel', TimelinePanel);

    function TimelinePanel(library) {
        var currentNumber = 1;
        var items = [];
        function load(programmes, channels) {
            for(var i = 0; i < 2; i++) {
                if(!angular.isUndefined(programmes[i][currentNumber])){
                    var item = programmes[i][currentNumber];
                    item.channel = channels[i];
                    items.push(item);
                }
            }
            currentNumber++;
            return items;
        }
        return {
            scope: {
                channels: '=channels',
                schedules: '=schedules'
            },
            link: function (scope, element, attrs) {
                if (!scope.schedules) {
                    return;
                }
                scope.load = function() {
                    scope.items = load(scope.schedules, scope.channels);
                };
                scope.load();
                //scope.items = load(scope.programmes);
            },
            templateUrl: 'widgets/panels/timeline-panel.html'
        };
    }
})();
