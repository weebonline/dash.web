(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('descriptionTab', DescriptionTab);

    function DescriptionTab(library) {
        var currentNumber = 0;
        var items = [];
        function load(programmes) {
            for(var i = 0; i < 4; i++) {
                if(!angular.isUndefined(programmes[i][currentNumber])){
                    items.push(programmes[i][currentNumber]);
                }
            }
            currentNumber++;
            return items;
        }
        return {
            scope: {
                channels: '=channels',
                schedules: '=schedules'
            },
            link: function (scope, element, attrs) {
                console.log(scope.schedules);
                if (!scope.schedules) {
                    return;
                }
                scope.load = function() {
                    scope.items = load(scope.schedules);
                };
                scope.loadDescription = function(key) {
                    scope.programme = items[key];
                };
                scope.load();
                scope.loadDescription(0);
            },
            templateUrl: 'widgets/blocks/description-tab.html'
        };
    }
})();
