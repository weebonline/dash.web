(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('channelsBlock', ChannelsBlock);

    function ChannelsBlock() {
        return {
            scope: {channels: '=channels'},
            transclude: true,
            // controller: ['$scope', function ChannelsBlockController($scope) {
            //     $scope.items = $scope.channs;
            //     console.log($scope.items, $scope.channs, $scope.channels);


            // }],
            // link: function (scope, element, attrs) {
            //     if (scope.channels.length === 0) {
            //         scope.items = [ls()];
            //         return;
            //     }
            //     console.log(scope.channs, scope.items);
            //     scope.items = scope.channs;

            //     // scope.items = scope.channels;

            // },
            templateUrl: 'widgets/blocks/channels-block.html'
        };
    }

//doc ready function
function ls() {
    return [
        {
            src: '/img/thumb/cmx2.png'
        },
        {
            src: '/img/thumb/hbo.png'
        },
        {
            src: '/img/thumb/tlc.png'
        },
        {
            src: '/img/thumb/hbo2.png'
        }
    ];
}

})();
