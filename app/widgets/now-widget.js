(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('nowWidget', NowWidget);


    //NowWidget.$inject = ['$scope'];

    function NowWidget() {
        return {
            scope: {
                schedules: '=schedules',
                channels: '=channels',
                watch: "=watch"
            },
            templateUrl: 'widgets/now-widget.html'
        };
    }
})();
