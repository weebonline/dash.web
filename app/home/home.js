'use strict';

angular.module('theWeeb.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'home/home.html',
        controllerAs: 'vm',
        controller: 'HomeController'
    });
}])

.controller('HomeController', HomeController);



HomeController.$inject = ['$location', '$sce'];

function HomeController($location, $sce) {
    var vm = this;
    var id = 'null';
    var query = $location.search();
    if(query.hasOwnProperty("id")){
        id = query.id;
    }
    vm.id = id;

    vm.url = {
        "mp4": "http://10.0.10.107:9001/api/" + id,
        "webm": "http://10.0.10.107:9001/api/" + id,
        "ogg": "http://10.0.10.107:9001/api/" + id
    };

    console.log(vm.url);

    vm.url.mp4 = $sce.trustAsResourceUrl(vm.url.mp4);
    vm.url.webm = $sce.trustAsResourceUrl(vm.url.webm);
    vm.url.ogg = $sce.trustAsResourceUrl(vm.url.ogg);
    var loaded = false;
    vm.loaded = function() {
        if (loaded) return;
        loaded = true;
        console.log("video");
        videojs("video", {
            plugins: {
                chromecast: {
                    appId: undefined,
                    metadata: {
                        title: "Title",
                        subtitle: "Subtitle"
                    }
                }
            }
        });
        Dash.createAll()
    }


}
