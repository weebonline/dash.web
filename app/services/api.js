(function() {
    'use strict';

    angular
        .module('theWeeb.api', ['angular-jwt'])
        .config(function ($httpProvider, jwtOptionsProvider) {
          jwtOptionsProvider.config({
            tokenGetter: function() {
              return localStorage.getItem('id_token');
            },
            whiteListedDomains: ['api.dash.docker']
          });
            $httpProvider.interceptors.push('jwtInterceptor');
          })
        .factory('api', ApiService);

    ApiService.$inject = ['$http'];

    function ApiService($http) {
        //var url = 'http://api.weeb.online';
        var url = 'http://api.weeb.online';
        url = 'http://api.dash.docker:8080';
        var data = {};
        var service = {
            url: url,
            tv: tv,
            genre: genre,
            channels: channels,
            user: user,
            get: get
        };
        return service;

        ////////////var defer = $q.defer()
        function tv() {
            return $http.get(service.url + "/api/tv")
                .then(success)
                .catch(failed);
        }

        function genre(genre) {
            var uri = service.url + "/api/tv/genres";
            if (genre !== undefined) {
                uri += "/" + genre;
            }
            return $http.get(uri)
                .then(success);
        }

        function channels(genre) {
            var uri = service.url + "/api/tv/channels";
            if (genre !== undefined) {
                uri += "?genre=" + genre;
            }
            return $http.get(uri)
                .then(success);
        }

        function user() {
            return $http.get(service.url + "/user")
                .then(success)
                .catch(failed);
        }

        function get(link) {
            return $http.get(url + link)
                .then(success);
        }

        function success(response) {
            var contents = response.data;
            if (Array.isArray(contents)) {
                var responses = [];
                angular.forEach(contents, function(data) {
                    this.push(new Api($http, data));
                }, responses);
                return responses;
            }
            return new Api($http, contents);
        }

        function failed(error) {
            console.log('XHR Failed for load.', error.data);
        }
    }

    function Api($http, content) {

        var api = content;
        content.get = get;
        content.post = post;
        angular.forEach(content.links, function(link) {
            this[link.rel] = link;
            //api[link.rel] = link;
        }, content.links);

        return content;
        ////////////var defer = $q.defer()

        function get(link) {
            var hateoasLink = content.links[link];
            switch (hateoasLink.method) {
                case 'GET':
                    return $http.get(hateoasLink.href)
                        .then(success);
            }
        }

        function post(link, data) {
            var hateoasLink = content.links[link];
                return $http.post(hateoasLink.href, data)
                    .then(success)
                    .catch(failed);
        }

        function success(response) {
            var contents = response.data;
            if (Array.isArray(contents)) {
                var responses = [];
                angular.forEach(contents, function(data) {
                    this.push(new Api($http, data));
                }, responses);
                return responses;
            }
            return new Api($http, contents);
        }

        function failed(error) {
            console.error(error);
        }
    }

    var HateoasLink = {
        rel: '',
        href: '',
        method: ''
    };
})();
