(function() {
    'use strict';

    angular
        .module('theWeeb.logger', [])
        .factory('logger', LoggerService);

    LoggerService.$inject = ['socketFactory'];

    function LoggerService(socketFactory) {
        var myIoSocket = io.connect('127.0.0.1:7080');

        var mySocket = socketFactory({
            ioSocket: myIoSocket
        });

        return mySocket;
    }
})();
