(function() {
    'use strict';

    angular
        .module('theWeeb.entertainment', [])
        .factory('entertainment', EntertainmentService);

    EntertainmentService.$inject = ['$http'];

    function EntertainmentService($http) {
        var url = 'http://10.0.10.107:9005/tv/';
        var data = {};
        var service = {
            url: url,
            schedule: schedule,
        };
        return service;

        ////////////var defer = $q.defer()

        function schedule(channel) {
            return $http.get(service.url + channel)
                .then(success)
                .catch(failed);
        }

        function success(response) {
            return response.data;
        }

        function failed(error) {
            console.log('XHR Failed for load.' + error.data);
        }
    }
})();
