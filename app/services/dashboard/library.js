(function() {
    'use strict';

    angular
        .module('theWeeb.service', [])
        .factory('library', LibraryService);

    LibraryService.$inject = ['$http'];

    function LibraryService($http) {
        var url = 'http://10.0.10.107:9000/content-directory/browse';
        var data = {};
        var service = {
            url: url,
            load: load,
            loadMetadata: loadMetadata,
            data: data
        };
        return service;

        ////////////

        function load(id) {
            return $http.get(this.url + "?objectId=" + id)
                .then(success)
                .catch(failed);
        };

        function loadMetadata(id) {
            return $http.get(this.url + "?flag=BrowseMetadata&objectId=" + id)
                .then(success)
                .catch(failed);
        };

        function success(response) {
            return response.data;
        };

        function failed(error) {
            console.log('XHR Failed for load.' + error.data);
            //logger.error('XHR Failed for getAvengers.' + error.data);
        };
    }
})();
