(function() {
    'use strict';

    angular
        .module('theWeeb.library', ['ngRoute'])

        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/library', {
                templateUrl: 'views/library.html',
                controller: 'LibraryController',
                controllerAs: 'vm',
                resolve: {
                    loadLibrary: function(library) {
                        return library.load('b3e079f8faa280eda68cddd234507e87');
                    }
                }
            });
        }])
})();


