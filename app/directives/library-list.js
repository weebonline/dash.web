(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('libraryList', LibraryList);

    LibraryList.$inject = ['library'];

    function LibraryList(library) {
        return {
            restrict: 'EA',
            transclude: true,
            replace: false,
            link: function (scope, element, attrs) {
                var load = function (id) {
                    if (id < 0) {
                        return;
                    }
                    library.load(id).then(function (data) {
                        var items = data.result;
                        scope.items = items;
                        console.log(items[0].parentId);
                    });
                    library.loadMetadata(id).then(function (data) {
                        var metadata = data.result;
                        if (metadata.parentId < 0) {
                            metadata.parentId = false;
                        }
                        scope.metadata = metadata;
                        console.log(metadata);
                    });
                };
                //scope.load = load;
                load(attrs.itemId);
            },
            templateUrl: 'directives/library-list.html'
        };
    }
})();
