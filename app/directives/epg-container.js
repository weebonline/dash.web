(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('epgContainer', EpgContainer);

    EpgContainer.$inject = ['entertainment', '$q'];

        function sleep(milliseconds) {
          var start = new Date().getTime();
          for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
              break;
            }
          }
        }

    function Grid(current, previous) {
        var grid = {
            'offset': 0,
            'length': 180,
            'column': 0
        };
        var now = new Date();
        var limit = 180;
        var length;
        var offset;

        offset = (current.startTime - now) / 1000 / 60;
        length = (current.endTime - current.startTime) / 1000 / 60;

        grid.offset = Math.round(offset);
        grid.length = Math.round(length);

        if (offset < 0) {
            grid.length = Math.ceil(length + offset);
        }

        if (grid.length > limit) {
            grid.length = limit;
        }

        if(!angular.isUndefined(previous)){
            length = (current.startTime - previous.startTime) / 1000 / 60;
            previous.grid.length = Math.round(length);
        }

        // ------------
        var columns = 12;

        grid.column = Math.round((grid.length / limit) * columns);
        if ((grid.offset + grid.length) > limit) {
            grid.column = Math.round(((limit - grid.offset) / limit) * columns);
        }
        return grid;
    }

    function Gridsad(current, previous) {
        var grid = {
            'offset': 0,
            'length': 180
        };
        var now = new Date();
        var length;
        var offset;

        offset = (current.startTime - now) / 1000 / 60;
        grid.offset = Math.floor(offset);

        length = (current.endTime - current.startTime) / 1000 / 60;
        grid.length = Math.floor(length);

        if ((grid.offset + grid.length) > 180) {
            grid.length = Math.floor(180 - grid.offset);
        }

        if(angular.isUndefined(previous)){
            return grid;
        }

        length = (current.startTime - previous.startTime) / 1000 / 60;
        if (previous.grid.offset < 0) {
            length = length + previous.grid.offset;
            previous.grid.offset = 0;
        }

        previous.grid.length = Math.floor(length);

        return grid;
    }

    var Item = function(properties) {
        var item = this;
        angular.forEach(properties, function(value, key) {
            this[key] = value;
        }, item);
    };

    var Container = function(items) {
        var container = [];
        angular.forEach(items, function(item) {
            var previous = container.length ? container[container.length - 1] : undefined;

            item.grid = new Grid(item, previous);
            this.push(item);
        }, container);


        return container;
    };

    function EpgContainer(entertainment, $q) {

        return {
            restrict: 'EA',
            transclude: true,
            replace: false,
            link: function (scope, element, attrs) {
                var load = function (channel) {
                    entertainment.schedule(channel).then(function (data) {
                        //sleep(300);
                        var response = data[channel];
                        if(angular.isUndefined(response)){
                            return;
                        }
                        var schedule = response.schedule;

                        var programs = [];
                        var now = new Date();
                        angular.forEach(schedule, function(properties, key) {
                            properties.startTime = new Date(properties.startTime);
                            properties.endTime = new Date(properties.endTime);

                            if (now < properties.endTime) {
                                if (now > properties.startTime) {
                                    properties.current = true;
                                    properties.elapsed = Math.floor((now - properties.startTime) / 1000 / 60);
                                    properties.progress = Math.floor(properties.elapsed / properties.duration * 100);
                                }
                                var theItem = new Item(properties);
                                programs.push(theItem);
                            }
                        });
                        var container = new Container(programs)
                        scope.programs = container;
                        return scope.programs;
                    });
                };
                load(attrs.channel);
            },
            templateUrl: 'directives/epg-container.html'
        };
    }



})();
