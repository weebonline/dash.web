(function () {
    'use strict';

    angular
        .module('theWeeb')
        .directive('libraryItem', LibraryItem);

    LibraryItem.$inject = ['library'];

    function LibraryItem(library) {
        return {
            restrict: 'EA',
            transclude: true,
            replace: false,
            link: function (scope, element, attrs) {
                console.log(attrs.item);
                scope.item = attrs.item;
            },
            templateUrl: 'directives/library-item.html'
        };
    }
})();
