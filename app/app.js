'use strict';

// Declare app level module which depends on views, and components
angular.module('theWeeb', [
    'ngRoute',
    'ngCookies',
    'theWeeb.api',
    'theWeeb.auth',
    'theWeeb.learn',
    'stBlurredDialog',
    'angularModalService',
    'angular-storage'
    // 'theWeeb.logger',
    // 'btford.socket-io'

]).config(['$routeProvider', function($routeProvider) {
    $routeProvider

        .when('/dashboard', {
            templateUrl: 'views/portlets.html',
            controller: 'PortletsController',
            controllerAs: 'vm'
        })

        .when('/dashboard/:genre', {
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardController',
            controllerAs: 'vm'
        })

        .when('/subscription', {
            controller: 'SubscriptionController',
            templateUrl: 'views/subscription/subscription.html',
            controllerAs: 'vm'
        })

        .when('/subscription/top-up', {
            controller: 'TopUpController',
            templateUrl: 'views/subscription/top-up.html',
            controllerAs: 'vm'
        })

        .when('/payment/one-off', {
            controller: 'OneOffController',
            templateUrl: 'views/payment/one-off.html',
            controllerAs: 'vm'
        })

        .when('/profile', {
            controller: 'ProfileController',
            templateUrl: 'views/profile/profile.html',
            controllerAs: 'vm'
        })

        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'views/login.view.html',
            controllerAs: 'vm'
        })

        .when('/login/reset-password', {
            controller: 'LoginController',
            templateUrl: 'views/authentication/reset-password.html',
            controllerAs: 'vm'
        })

        .when('/register', {
            controller: 'RegisterController',
            templateUrl: 'views/register.view.html',
            controllerAs: 'vm'
        });

        // .when('/logger', {
        //     templateUrl: 'views/logger/logger.html',
        //     controller: 'LoggerController',
        //     controllerAs: 'vm'
        // })

        // .otherwise({redirectTo: '/'});
}])

// angular.module('exceptionOverride', []).factory('$exceptionHandler', function () {
//   return function (exception, cause) {
//     Bugsnag.notifyException(exception, {diagnostics:{cause: cause}});
//   };
// });

angular.module('theWeeb.learn', [
    'ngRoute'

]).config(['$routeProvider', function($routeProvider) {
    $routeProvider

        .when('/learn', {
            controller: 'LearnController',
            templateUrl: 'views/learn/learn-tv.html',
            controllerAs: 'vm'
        })

        .when('/learn/:device', {
            controller: 'LearnController',
            templateUrl: 'views/learn/learn-tv.html',
            controllerAs: 'vm'
        });

        // .when('/logger', {
        //     templateUrl: 'views/logger/logger.html',
        //     controller: 'LoggerController',
        //     controllerAs: 'vm'
        // })
}]);


angular.module('theWeeb.auth', [
    'ngRoute',
    'auth0.lock',
    'angular-jwt',

]).config([
    '$routeProvider',
    '$httpProvider',
    'lockProvider',
    'jwtOptionsProvider',
    function ($routeProvider, $httpProvider, lockProvider, jwtOptionsProvider) {
        lockProvider.init({
            clientID: 'VT4gd3Ccg9geInpc8LPfrshsXURfqiSH',
            domain: 'weeb.eu.auth0.com'
            // options: {auth: {redirectUrl: location.origin + "#/dashboard/movies"}}
        });

        // Configuration for angular-jwt
        jwtOptionsProvider.config({
            tokenGetter: function() {
                return localStorage.getItem('id_token');
            },
            whiteListedDomains: ['api.dash.docker']
        });
        // Add the jwtInterceptor to the array of HTTP interceptors
        // so that JWTs are attached as Authorization headers
        $httpProvider.interceptors.push('jwtInterceptor');

        $routeProvider

            .when( '/', {
              controller: 'HomeController',
              templateUrl: 'views/home.html',
              reloadOnSearch: false
            });

}]).run(function($http, store, $rootScope, authService, authManager) { // instance-injector
    $rootScope.authService = authService;
    authService.registerAuthenticationListener();
      // Use the authManager from angular-jwt to check for the user's authentication
      // state when the page is refreshed and maintain authentication
    authManager.checkAuthOnRefresh();
      // Listen for 401 unauthorized requests and redirect the user to the login page
    authManager.redirectWhenUnauthenticated();
    // var token = store.get('user.token');
    // console.log(token);
    // $http.defaults.headers.common.Authorization = 'Bearer ' + token;
});
