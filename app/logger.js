var redis = require('redis');
var util = require("util");
var app = require('http').createServer();

var sock = require('socket.io');
var io = sock.listen(app);

// hgetall converts its replies to an Object.  If the reply is empty, null is returned.
function replyToObject(reply) {
    var obj = {}, j, jl, key, val;

    if (reply.length === 0 || !Array.isArray(reply)) {
        return null;
    }

    for (j = 0, jl = reply.length; j < jl; j += 2) {
        key = reply[j].toString('binary');
        val = reply[j + 1];
        obj[key] = val;
    }
    return obj;
}

app.listen(7080);

var latest = 0;

io.sockets.on('connection', function (socket) {

    const subscribe = redis.createClient(undefined, "127.0.0.1")
    const pub = redis.createClient(undefined, "127.0.0.1")



    var client = redis.createClient();
    client.on("error", function (err) {
        console.log("Error " + err);
    });

    // client.lrange(["asda", -10, -1], function(err, reply) {
    //     // reply is null when the key is missing
    //     console.log(err);
    //     var items = replyToObject(reply);
    //     for (var key in items) {
    //         console.log(items[key]);
    //         socket.emit('message', {
    //             logs: items[key],
    //         });
    //     }
    //   socket.emit('message', {
    //     logs: replyToObject(reply),
    //   });
    // });

  socket.on('asda.get', function (data) {

    client.llen("asda", function(err, length) {
        if (latest === 0) {
            latest = length - data.max;
        }
        var message = {
            logs : false,
            length: length,
            latest: latest
        };


        if (length < latest + data.count) {
            socket.emit('message', message);
            return;
        }
        var stop = latest + data.max;

        var start = latest;
        console.log(latest, length, data.count, start, stop);
        message.latest = latest = stop;
        client.lrange(['asda', start, stop], function(err, reply) {
                // reply is null when the key is missing
                //console.log(err);
                var items = replyToObject(reply);
                for (var key in items) {
                    console.log(items[key]);
                    message.logs = items[key];
                    socket.emit('message', message);
                }
            });
        });
  });
        // client.lpop(["asda"], function(err, reply) {
        //     // reply is null when the key is missing
        //     var item = replyToObject(reply);
        //     console.log(item);
        //     socket.emit('message', {
        //         logs: item,
        //     });
        // });



client.on("monitor", function (time, args) {
    console.log(args[2]);
    socket.emit('message', {
        logs: args[2],
    });
});

   /**
    * Connect to redis channel and I trigger init event
    */
   socket.on("subscribe", function (channel) {
        subscribe.subscribe(channel);
        socket.emit("init", "woww");
    });

    socket.on('unsubscribe', function (obj) {
        subscribe.unsubscribe(obj.channel);
    });

    /*
     * Redis use subcribe on for send new message in a channel
     */
    subscribe.on("message", function (channel, data) {
        /*
         *  Socket.io use send function for trigger an event into default channel 'message'
         */
        socket.send(data);
    });
});
