(function() {
    'use strict';

    var navItems = [
        {
            'slug': 'dashboard',
            'name': 'Dashboard',
            'icon': 'l-basic-home',
            'isActive': false,
            'subItems': []
        },
    ];
    function UserProfile(profile, $http) {
        var userProfile = {
            authenticated: false,
            userName: 'userName',
            avatar: null,
            load: function(profile) {
                if (profile.nickname !== undefined) {
                    return userProfile.authService(profile);
                }
                userProfile.random();
            },
            random: function() {
                $http.get('https://randomuser.me/api/').then(userProfile.randomUser);
            },
            randomUser: function(response) {
                var user = response.data.results.pop();
                user.first = user.name.first.charAt(0).toUpperCase() + user.name.first.slice(1);
                user.last = user.name.last.charAt(0).toUpperCase() + user.name.last.slice(1);
                if (userProfile.authenticated === false) {
                    userProfile.userName = user.first + user.last;
                    userProfile.avatar = user.picture.thumbnail;
                }
            },
            authService: function(profile) {
                userProfile.authenticated = true;
                userProfile.userName = profile.nickname;
                userProfile.avatar = profile.picture;
            },
            userProfileSet: function(event, profile) {
                return userProfile.authService(profile);
            },
            userProfileUnset: function(event, profile) {
                userProfile.authenticated = false;
                userProfile.random();
            }
        };
        userProfile.load(profile);
        return userProfile;
    }

    angular
        .module('theWeeb')
        .controller('LeftSidebarController', LeftSidebarController);

    LeftSidebarController.$inject = ['$scope', '$routeParams', 'api', '$location', '$http', 'authService'];

    function LeftSidebarController($scope, $routeParams, api, $location, $http, authService) {
        var vm = this;
        $scope.profile = new UserProfile(authService.userProfile, $http);
        // Listen for the user profile being set when the user  logs in and update it in the view
        $scope.$on('userProfileSet', $scope.profile.userProfileSet);
        $scope.$on('userProfileUnset', $scope.profile.userProfileUnset);

        $scope.init = function() {
            $('body').dynamic();
        };

        api.genre().then(function (genres) {
            angular.forEach(genres, function(genre, key) {

                genres[key].isActive = false;
                if (genre.slug === $routeParams.genre) {
                    genres[key].isActive = true;
                    genres.isActive = true;
                }
            });
            $scope.navItems.push({
                'slug': 'dashboard',
                'name': 'Genres',
                'icon': 'l-arrows-circle-down',
                'isActive': true,
                'subItems': genres
            });

        });
        $scope.navItems = navItems;

        $scope.setLocation = function(location) {
            $location.path(location);
        };
    }

})();
