(function(recurly) {
    'use strict';

    angular
        .module('theWeeb')
        .controller('OneOffController', OneOffController);

    OneOffController.$inject = ['api', '$location', 'stBlurredDialog', '$sce', '$routeParams', 'ModalService'];

    function OneOffController(api, $location, stBlurredDialog, $sce, $routeParams, ModalService) {
        var vm = this;
        // sandbox data
        vm.cc = {
             'first_name': 'asd',
             'last_name': 'asd',
             number: '4111111111111111',
             month: '2',
             year: '18',
             cvv: ''
        };
        recurly.configure('sjc-GUYRbHwrqyd2xqauvbBnLK');
        vm.pay = function () {
            recurly.token(vm.cc, function tokenHandler (err, token) {
              if (err) {
                console.log(err);
                // handle error using err.code and err.fields
              } else {
                console.log(token);
                // handle success using token.id
              }
            });
        };
    }

})(recurly);
