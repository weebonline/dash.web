(function() {
    'use strict';

    angular
        .module('theWeeb')
        .controller('DialogController', DialogController);
    // Create a controller for your modal dialog

    DialogController.$inject = ['$scope', 'stBlurredDialog'];

    function DialogController($scope, stBlurredDialog) {
        // Get the data passed from the controller
        console.log($scope, stBlurredDialog);
        $scope.dialogData = stBlurredDialog.getDialogData();
    }
})();
