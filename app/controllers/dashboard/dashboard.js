(function() {
    'use strict';

    angular
        .module('theWeeb')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', 'api', '$location', 'stBlurredDialog', '$sce', '$routeParams'];

    function DashboardController($scope, api, $location, stBlurredDialog, $sce, $routeParams) {
        var vm = this;
        vm.objectId = 0;
        vm.channels = [];
        $scope.channels = [];
        vm.schedules = [];
        vm.programmes = [];
        vm.timeline = [];
        vm.description = {
            namePl: '',
            description: ''
        };
        vm.colors = ['panel-info', 'panel-success', 'panel-danger', 'dark panel-default'];

        var videoModal = new BlurredVideoModal(stBlurredDialog, $sce);
        vm.watch = videoModal.watch;

        vm.loadDescription = function(channel) {
            vm.description = channel.emissions[0];
        };

        var loadSchedule = function (channel, key) {
            channel.emissions = [];
            channel.get('schedule').then(function (emissions) {
                var now = new Date();
                angular.forEach(emissions, function(emission) {
                    emission.startTime = new Date(emission.startTime);
                    emission.endTime = new Date(emission.endTime);
                    emission.tillMinutes = Math.floor((now - emission.startTime) / 1000 / 60);
                    emission.channelName = channel.name;
                    //if (emission.tillMinutes < 0) {
                        vm.timeline.push(emission);
                    //}
                });
                channel.emissions = emissions;
                vm.channels.push(channel);
                vm.schedules.push(emissions);
                if (key === 0) {
                    vm.loadDescription(channel);
                }
            }).catch(function(error){
                vm.channels.push(channel);
                console.warn('couldn\'t get schedule for ', channel.name);
            });
        };

        var loadSchedules = function (channels) {
            angular.forEach(channels, function(channel, key) {
                loadSchedule(channel, key);
            });
        };

        var loadChannels = function (channels) {
            var groups = [];
            var group = [];
            angular.forEach(channels, function(channel, key) {
                group.push(channel);
                if (group.length === 4) {
                    groups.push(group);
                    group = [];
                }
            });

            console.log(groups);
            $scope.channels = groups;
        };

        api.genre($routeParams.genre)
            .then(function (genre) {
                genre.get('channels').then(function (channels) {
                    var begin = 0;
                    var search = $location.search();
                    if (search.hasOwnProperty("page")) {
                        begin = Math.min((search.page - 1) * 4, channels.length - 4);
                    }
                    loadChannels(channels);
                    channels = channels.slice(begin, begin + 4);
                    loadSchedules(channels);
                });
            });
    }

    var Container = function(programme) {
        var container = {
            schedule: [],
            channel: programme.channel
        };
        angular.forEach(programme.schedule, function(item) {
            this.push(new Item(item, container.channel));
        }, container.schedule);
        return container;
    };

    var Item = function(properties, channel) {
        var item = this;
        properties.startTime = new Date(properties.startTime * 1000);
        properties.endTime = new Date(properties.endTime * 1000);
        properties.current = false;
        properties.elapsed = 0;
        properties.progress = 100;
        properties.namePl = properties['name_pl'];
        var now = new Date();
        if (now < properties.endTime && now > properties.startTime) {
            properties.current = true;
            properties.elapsed = Math.floor((now - properties.startTime) / 1000 / 60);
            properties.progress = Math.floor(properties.elapsed / properties.duration * 100);
        }
        properties.tillMinutes = Math.floor((now - properties.startTime) / 1000 / 60);
        item.ItemType = function(type, subtype) {
            switch(type) {
                case 'Film fabularny':
                    switch (subtype) {
                        case 'Komedia':
                            return 'fa-smile-o';
                        case 'Dramat obyczajowy':
                            return 'fa-tint';
                        case 'Komediodramat':
                            return 'fa-meh-o';
                        case 'Film fantastyczny':
                            return 'fa-fighter-jet';
                        case 'Film przygodowy':
                            return 'fa-desktop';
                        case 'Film animowany':
                            return 'fa-child';
                        default:
                            return 'fa-film';
                    }
                default:
                    return 'fa-calendar-o';
            }
        };
        properties.itemType = item.ItemType(properties.type, properties.subtype);

        angular.forEach(properties, function(value, key) {
            this[key] = value;
        }, item);
        item.channel = channel;
    };

    var BlurredVideoModal = function(stBlurredDialog, $sce) {
        return {
            watch: function (channel) {
                // var channel = vm.channels[selected];
                var streams = channel.get("streams").then(function (streams) {
                    var url = {};
                    url.browser = $sce.trustAsResourceUrl(streams.links.browser.href);
                    url.chromecast = streams.links.chromecast.href;
                    var loaded = false;
                    url.loaded = function() {
                        if (loaded) return;
                        loaded = true;
                        var player = videojs("video", {
                            plugins: {
                                chromecast: {
                                    appId: undefined,
                                    metadata: {
                                        title: 'Title',
                                        subtitle: 'Subtitle',
                                        chromeUri: url.chromecast,
                                        contentType: 'video/mp4'
                                    }
                                }
                            }
                        });
                        player.controls(true);
                        player.autoplay(true);
                        if (streams.playbackPosition > 0) {
                            player.currentTime(streams.playbackPosition * 60);
                        }

                    };

                    stBlurredDialog.subscribe(function (isOpen) {
                        if (!isOpen) {
                            var player = videojs('video');
                            player.dispose();
                        }
                    })
                    stBlurredDialog.open('views/dashboard/video.html', {
                        url: url,
                        msg: 'Hello from the controller!' //streams
                    });
                });
            }
        };
    };

})();
