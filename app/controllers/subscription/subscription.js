(function() {
    'use strict';

    angular
        .module('theWeeb')
        .controller('SubscriptionController', SubscriptionController);

    SubscriptionController.$inject = ['api', '$location', 'stBlurredDialog', '$sce', '$routeParams', 'ModalService'];

    function SubscriptionController(api, $location, stBlurredDialog, $sce, $routeParams, ModalService) {
        var vm = this;
        var customerProgress = new ProgressBar.Circle('#customer-exp', {
            color: '#0a97b9',
            strokeWidth: 2,
            fill: '#d0f1f9',
            duration: 4000,
            easing: 'bounce'
        });
        customerProgress.animate(0.8);
    }

})();
