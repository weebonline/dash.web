(function () {
    'use strict';

    angular
        .module('theWeeb')
        .controller('ResetPasswordController', ResetPasswordController);

    ResetPasswordController.$inject = ['api', '$location', 'AuthenticationService', 'FlashService', 'store', 'ModalService'];
    function ResetPasswordController(api, $location, AuthenticationService, FlashService, store, ModalService) {
        var vm = this;
        console.log("ResetPassword controller on...");

        vm.reset = function () {
            console.log("Reset action...");
            vm.dataLoading = true;
            console.log(vm.username);
            api.user().then(function (userService) {
                console.log(userService);
                userService.post('reset', {username: vm.username}).then(
                    function (response) {
                        console.log(response.response);
                    }, function (error) {
                        console.log(error);
                    });
            });

            // AuthenticationService.Login(vm.username, vm.password, function (response) {
            //     if (response.success) {
            //         AuthenticationService.SetCredentials(vm.username, vm.password);
            //         $location.path('/');
            //     } else {
            //         console.log(response.message);
            //         FlashService.Error(response.message);
            //         vm.dataLoading = false;
            //     }
            // });
        };
    }

})();
