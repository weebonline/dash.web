﻿(function () {
    'use strict';

    angular
        .module('theWeeb')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['api', 'UserService', '$location', '$rootScope', 'FlashService'];
    function RegisterController(api, UserService, $location, $rootScope, FlashService) {
        var vm = this;

        vm.register = register;

        function register() {
            vm.dataLoading = true;
            console.log("registering...");
            console.log(vm.user);
            api.user().then(function (userService) {
                console.log(userService);
                userService.post('profiles', vm.user).then(
                    function (response) {
                        console.log(response);
                    }, function (error) {
                        console.log(error);
                    });
            });

            // UserService.Create(vm.user)
            //     .then(function (response) {
            //         if (response.success) {

            //             FlashService.Success('Registration successful', true);
            //             //$location.path('/login');
            //         } else {
            //             console.log(response.message);
            //             FlashService.Error(response.message);
            //             vm.dataLoading = false;
            //         }
            //     });
        }
    }

})();
