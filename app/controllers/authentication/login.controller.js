﻿(function () {
    'use strict';

    angular
        .module('theWeeb')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['api', '$location', 'AuthenticationService', 'FlashService', 'store', 'ModalService', 'close'];
    function LoginController(api, $location, AuthenticationService, FlashService, store, ModalService, close) {
        var vm = this;
        console.log("Login controller on...");
        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();


        vm.showResetPassword = function() {
            close();
            ModalService.showModal({
                templateUrl: 'views/authentication/reset-password.html',
                controller: 'ResetPasswordController',
                controllerAs : 'vm'
            }).then(function(modal) {
                modal.element.modal();
            });
        };

        function login() {
            console.log("Login action...");
            vm.dataLoading = true;
            console.log(vm.username, vm.password);
            api.user().then(function (userService) {
                console.log(userService);
                userService.post('session', {username: vm.username, password: vm.password}).then(
                    function (response) {
                        console.log(response.response);
                        store.set('user.token', response.response.token);
                    }, function (error) {
                        console.log(error);
                    });
            });

            // AuthenticationService.Login(vm.username, vm.password, function (response) {
            //     if (response.success) {
            //         AuthenticationService.SetCredentials(vm.username, vm.password);
            //         $location.path('/');
            //     } else {
            //         console.log(response.message);
            //         FlashService.Error(response.message);
            //         vm.dataLoading = false;
            //     }
            // });
        };
    }

})();
