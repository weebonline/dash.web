(function() {

  'use strict';

  angular
    .module('theWeeb')
    .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'authService'];

    function HomeController($scope, authService) {
      $scope.profile = authService.userProfile;
      $scope.$on('userProfileSet', function(event, userProfile) {
        $scope.profile = userProfile;
      });
    }

})();
