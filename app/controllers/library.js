(function() {
    'use strict';

    angular
        .module('theWeeb.library', ['ngRoute'])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/library', {
                templateUrl: 'views/library.html',
                controller: 'LibraryController',
                controllerAs: 'vm',
                resolve: {
                    loadLibrary: function(library) {
                        return library.load('b3e079f8faa280eda68cddd234507e87');
                    }
                }
            });
        }])
        .controller('LibraryController', LibraryController);

    LibraryController.$inject = ['loadLibrary', '$location', 'entertainment'];

    function LibraryController(loadLibrary, $location, entertainment) {
        var vm = this;
        vm.objectId = 0;
        vm.items = loadLibrary.result;
        vm.load = function (channel) {
            entertainment.schedule(channel).then(function (data) {
                sleep(300);
                vm.channelItems = data[channel];
                console.log(vm.channelItems);

            });
        };
        console.log(vm.items);
        var query = $location.search();
        if(query.hasOwnProperty('id')){
            vm.objectId = query.id;
        }
    }


        function sleep(milliseconds) {
          var start = new Date().getTime();
          for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
              break;
            }
          }
        }

})();
