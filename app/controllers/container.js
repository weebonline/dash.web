(function() {
    'use strict';

    angular
        .module('theWeeb.library', ['ngRoute'])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/container', {
                templateUrl: 'views/container.html',
                controller: 'ContainerController',
                controllerAs: 'vm'
            });
        }])
        .controller('ContainerController', ContainerController);

    ContainerController.$inject = ['entertainment', 'library', '$location'];

    function ContainerController(entertainment, library, $location) {
        var vm = this;
        vm.objectId = 0;
        vm.items = [];
        var query = $location.search();
        if(query.hasOwnProperty('id')){
            vm.objectId = query.id;
        }

        function sleep(milliseconds) {
          var start = new Date().getTime();
          for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
              break;
            }
          }
        }

        function load (channel) {
            entertainment.schedule(channel).then(function (data) {
                var item = data[channel];
                console.log(item);
                sleep(1000);
                vm.items.push(item);
            });
        }

        vm.load = load;

        //load(attrs.channel);

    }

})();
