(function() {
    'use strict';


    angular
        .module('theWeeb')
        .controller('LoggerController', LoggerController);

    LoggerController.$inject = ['$scope', 'logger'];

    function LoggerController($scope, socket) {

        var logs = [];
        var vm = this;
        vm.logs = [];
        var levels = vm.levels = LogLevels;
        var channels = vm.channels = LogChannels;



        var filters = [levels, channels];

        vm.setLevel = function (level) {
            levels.setLevel(level);
            filterLogs();
        },

        vm.setChannel = function (channel) {
            channels.setChannel(channel);
            filterLogs();
        };

        var filter = function(entry) {
            var accepted = true;
            angular.forEach(filters, function(filter) {
                if (!filter.accept(entry)) {
                    accepted = false;
                }
            });
            if (accepted) {
                vm.logs.push(entry);
            }
        };

        var filterLogs = function() {
            vm.logs = [];
            angular.forEach(logs, function(entry) {
                filter(entry);
            });
        };

        var add = function(item) {
            var entry = logEntry(item);
            logs.push(entry);
            channels.add(item.channel);
            filter(entry);
        };

        var addItem = function(log) {
            try {
                var item = JSON.parse(log);
                add(item);
            } catch(e) {
                console.log(log); //error in the above string(in this case,yes)!
            }
        };
        socket.on('message', function (message, data) {
            if (!message.logs) {
                return;
            }
            if (typeof message.logs  === 'object') {
                for (var log in message.logs) {
                    addItem(message.logs[log]);
                }
            } else {
                addItem(message.logs);
            }
        });

        function getLogs() {
            setTimeout(function(){
                console.log('waiting...');
                socket.emit('asda.get', { count: 1, max: 50 });
                getLogs();
            }, 500);
        }
        getLogs();
    }



        function sleep(milliseconds) {
          var start = new Date().getTime();
          for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
              break;
            }
          }
        }

    var LogChannels = {
        channels: [
            'ALL'
        ],

        channel: 'ALL',

        accept: function(entry) {
            return this.channel === 'ALL' || entry.channel === this.channel;
        },

        setChannel: function (channel) {
            this.channel = channel;
        },

        add: function (channel) {
            if (this.channels.indexOf(channel) === -1) {
                this.channels.push(channel);
            }
        },

        isActive: function(channel) {
            return channel === this.channel;
        }
    };

    var LogLevels = {
        levels: {
            'DEBUG': 100,
            'INFO': 200,
            'NOTICE': 250,
            'WARNING': 300,
            'ERROR': 400,
            'CRITICAL': 500,
            'ALERT': 550,
            'EMERGENCY': 600,
        },

        level: 100,

        accept: function(entry) {
            return entry.level >= this.level;
        },

        setLevel: function (level) {
            this.level = level;
        },

        isActive: function(level) {
            return level === this.level;
        }
    };

/*
channel: "DEMO"
context: Array[0]
datetime: Object
extra: Array[0]
level: 200
level_name: "INFO"
message: "listening... [10.0.10.107:9020]"

DEBUG => $this->ansi->color(SGR::COLOR_FG_WHITE)->get(),
INFO => $this->ansi->color(SGR::COLOR_FG_GREEN)->get(),
NOTICE => $this->ansi->color(SGR::COLOR_FG_CYAN)->get(),
WARNING => $this->ansi->color(SGR::COLOR_FG_YELLOW)->get(),
ERROR => $this->ansi->color(SGR::COLOR_FG_RED)->get(),
CRITICAL => $this->ansi->color(SGR::COLOR_FG_RED)->underline()->get(),
ALERT => $this->ansi->color(array(SGR::COLOR_FG_WHITE, SGR::COLOR_BG_RED_BRIGHT))->get(),
EMERGENCY => $this->ansi->color(SGR::COLOR_BG_RED_BRIGHT)->blink()->color(SGR::COLOR_FG_WHITE)->get(),
 */
    function logEntry(entry) {
        var level = entry['level_name'];
        var color = 'black';
        switch (level) {
            case 'DEBUG': color = 'blue';
                break;
            case 'INFO': color = 'green';
                break;
            case 'NOTICE': color = 'brown';
                break;
            case 'WARNING': color = 'yellow';
                break;
            case 'ERROR': color = 'red';
                break;
            case 'CRITICAL': color = 'red';
                break;
            case 'ALERT': color = 'red';
                break;
            case 'EMERGENCY': color = 'red';
                break;
        }
        entry.elapsed = Math.floor((Date.now() - new Date(entry.datetime.date)) / 1000);
        switch (true) {
            case entry.elapsed > 60 * 60 * 24:
                entry.elapsed = Math.floor(entry.elapsed / (60 * 60 *24)) + 'd';
                break;
            case entry.elapsed > 60 * 60:
                var h = Math.floor(entry.elapsed / (60 * 60));
                var m = Math.floor((entry.elapsed / (60)) - h * 60);
                entry.elapsed = h +'h ' + m + 'm';
                break;
            case entry.elapsed > 60:
                entry.elapsed = Math.floor(entry.elapsed / 60) + 'm';
                break;
        }
        entry.color = color;
        entry.contextShow = false;
        entry.isContext = function () {
            return entry.context.length !== 0;
        }
        return entry;
    }
})();
