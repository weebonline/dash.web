(function() {
    'use strict';

    angular
        .module('theWeeb')
        .controller('RightSidebarController', RightSidebarController);

    RightSidebarController.$inject = ['$scope', '$routeParams', 'api', '$location'];

    function RightSidebarController($scope, $routeParams, api, $location) {
        var vm = this;
        vm.timelines = [];

        var loadSchedule = function (channel, key) {
            channel.get('schedule').then(function (schedules) {
                angular.forEach(schedules, function(schedule) {
                    schedule.channelName = channel.name;
                    this.push(schedule);
                }, vm.timelines);
            });
        };

        $scope.$on('$routeChangeSuccess', function (ev, current, prev) {
            if (current.params.genre) {
                api.channels(current.params.genre).then(function (channels) {
                    angular.forEach(channels, function(channel, key) {
                        loadSchedule(channel, key);
                    });
                });
            }
        });
    }

})();
