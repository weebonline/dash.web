(function() {
    'use strict';

    angular
        .module('theWeeb')
        .controller('NavbarController', NavbarController);

    function AuthModal(ModalService) {
        var auth = {
            register: null,
            showLogin: function() {
                ModalService.showModal({
                    templateUrl: "views/authentication/login.html",
                    controller: "LoginController",
                    controllerAs : "vm"
                }).then(function(modal) {
                    modal.element.modal();
                    // modal.close.then(function(result) {
                    //     auth.login  = result.login;
                    // });
                });
            },
            showRegister: function() {
                ModalService.showModal({
                    templateUrl: 'views/authentication/register.html',
                    controller: 'RegisterController',
                    controllerAs : 'vm'
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        auth.register  = result.register;
                    });
                });
            }
        };
        return auth;
    }

    NavbarController.$inject = ['$scope', 'ModalService', 'authService'];

    function NavbarController($scope, ModalService, authService) {
        $scope.authModal = new AuthModal(ModalService);
        $scope.authService = authService;
    }

})();
