'use strict';

// Declare app level module which depends on views, and components
angular.module('theWeeb', [
    'ngRoute',
    'theWeeb.logger',
    'btford.socket-io'

]).config(['$routeProvider', function($routeProvider) {
    $routeProvider

        .when('/logger', {
            templateUrl: 'views/logger/logger.html',
            controller: 'LoggerController',
            controllerAs: 'vm'
        })

        .otherwise({redirectTo: '/logger'});
}]);


