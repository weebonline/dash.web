### Settings ###
SHELL = /bin/bash
.DEFAULT_GOAL := run

### DOCKER-COMPOSE ###
.PHONY : run stop rm clean composer

run:
	docker-compose up -d

stop:
	docker-compose stop

rm:
	docker-compose rm -a -f

clean: stop rm

### COMPOSER ###

composer:
	npm install

### RANCHER-COMPOSE ###

.PHONY : upgrade confirm-upgrade

ENVIRONMENT?=development
BATCH_INTERVAL ?= 1000
BATCH_SIZE ?= 10

### RANCHER-COMPOSE ###

# Upgrades all services in the stack (i.e. docker-compose.yml)
upgrade:
	rancher-compose --env-file "config/$(ENVIRONMENT).env" \
	--file docker-compose.yml --file docker-compose.rancher.yml -r rancher-compose.yml up -d \
	--batch-size "$(BATCH_SIZE)" --interval "$(BATCH_INTERVAL)" \
	--upgrade --pull

# Confirm that the upgrade is complete and successful
confirm-upgrade:
	rancher-compose up -d --confirm-upgrade
